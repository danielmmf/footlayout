<section class="content-block" role="main">				
    <div class="row-fluid">					
        <article class="data-block">
            <div class="block-title"><h4>Parmera Vs Gambitas : </h4> Publico Pagante : 10.ooo torcedores . estadio do Parmera. </div>
            <div class="data-container">
                {?include file="foot/_campo.tpl"?}
            </div>
        </article>					
        <article class="data-block">
            <div class="block-title"><h4>Jogadores no campo</h4></div>
            <div class="data-container">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="alert warning-info">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>selecione um jogador </strong>para substituir.
                        </div>
                    </div>
                </div>
                <form class="form-vertical" method="POST">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th class="center"><input class="check_all" type="checkbox"></th>
                                <th>Jogadores</th>
                                <th>Rendimento</th>
                                <th>Faltas</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>                                       
                        <tbody>										
                            <tr>
                                <td class="center"><input type="checkbox"></td>
                                <td>Zelao</td>
                                <td>Esta rendendo bem</td>
                                <td class="center">6</td>
                                <td class="center">
                                    <a href="http://wbpreview.com/previews/WB0JJX0KG/tables.html#myModal" role="button" data-toggle="modal" class="btn btn-primary btn-mini">Edit</a>
                                    <a href="http://wbpreview.com/previews/WB0JJX0KG/tables.html#myModal" role="button" data-toggle="modal" class="btn btn-danger btn-mini delete">Delete</a>
                                </td>
                            </tr>
                            <tr class="odd gradeA">
                                <td class="center"><input type="checkbox"></td>
                                <td>Mussum</td>
                                <td>Machucado</td>
                                <td class="center">7</td>
                                <td class="center">
                                    <a href="http://wbpreview.com/previews/WB0JJX0KG/tables.html#myModal" role="button" data-toggle="modal" class="btn btn-primary btn-mini">Edit</a>
                                    <a href="http://wbpreview.com/previews/WB0JJX0KG/tables.html#myModal" role="button" data-toggle="modal" class="btn btn-danger btn-mini delete">Delete</a>
                                </td>
                            </tr>
                            <tr>
                                <td class="center"><input type="checkbox"></td>
                                <td>Tio Patinhas</td>
                                <td>Insano</td>
                                <td class="center">6</td>
                                <td class="center">
                                    <a href="http://wbpreview.com/previews/WB0JJX0KG/tables.html#myModal" role="button" data-toggle="modal" class="btn btn-primary btn-mini">Edit</a>
                                    <a href="http://wbpreview.com/previews/WB0JJX0KG/tables.html#myModal" role="button" data-toggle="modal" class="btn btn-danger btn-mini delete">Delete</a>
                                </td>
                            </tr>
                            <tr>
                                <td class="center"><input type="checkbox"></td>
                                <td>Pateta</td>
                                <td>perdido na partida</td>
                                <td class="center">3</td>
                                <td class="center">
                                    <a href="http://wbpreview.com/previews/WB0JJX0KG/tables.html#myModal" role="button" data-toggle="modal" class="btn btn-primary btn-mini">Edit</a>
                                    <a href="http://wbpreview.com/previews/WB0JJX0KG/tables.html#myModal" role="button" data-toggle="modal" class="btn btn-danger btn-mini delete">Delete</a>
                                </td>
                            </tr>                                    		
                        </tbody>
                    </table>
                </form>
            </div>
        </article>					
        <article class="data-block">
            <div class="block-title"><h4>Jogos Rolando</h4></div>
            <div class="data-container">
                <div class="row-fluid">
                    <form class="form-vertical" method="POST">
                        <div class="span5">
                            <label>
                                Mostrar 
                                <select name="length" class="span3">
                                    <option value="10" selected="selected">10</option>
                                    <option value="25">25</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select> Resultados
                            </label>
                        </div>
                        <div class="span7">
                            <label class="pull-right">Busca :  <input type="text"></label>
                        </div>
                    </form>
                </div>
                <table id="datatable" class="table table-striped table-bordered dataTable">
                    <thead>
                        <tr>
                            <th>Time</th>
                            <th>Adversario</th>
                            <th>Jogador</th>
                            <th>tempo</th>
                            <th>total gols</th>
                        </tr>
                    </thead>           
                    <tbody>
                        <tr>
                            <td>Parmera [5]</td>
                            <td>gambitas [0]</td>
                            <td>Opina</td>
                            <td class="center">1.7</td>
                            <td class="center">5</td>
                        </tr>
                        <tr class="gradeA even">
                            <td>Parmera [4]</td>
                            <td>gambitas [0]</td>
                            <td>Opina</td>
                            <td class="center">1.8</td>
                            <td class="center">4</td>
                        </tr>
                        <tr class="gradeA odd">
                            <td>Parmera [3]</td>
                            <td>gambitas [0]</td>
                            <td>Opina</td>
                            <td class="center">1.8</td>
                            <td class="center">3</td>
                        </tr>
                        <tr class="gradeA even">
                            <td>Parmera [2]</td>
                            <td>gambitas [0]</td>
                            <td>Opina</td>
                            <td class="center">1.9</td>
                            <td class="center">2</td>
                        </tr>
                    </tbody>
                </table>
                <div class="pagination pagination-right">
                    <ul>
                        <li><a href="http://wbpreview.com/previews/WB0JJX0KG/tables.html#">Prev</a></li>
                        <li><a href="http://wbpreview.com/previews/WB0JJX0KG/tables.html#">1</a></li>
                        <li class="active"><a href="http://wbpreview.com/previews/WB0JJX0KG/tables.html#">2</a></li>
                        <li><a href="http://wbpreview.com/previews/WB0JJX0KG/tables.html#">3</a></li>
                        <li><a href="http://wbpreview.com/previews/WB0JJX0KG/tables.html#">4</a></li>
                        <li><a href="http://wbpreview.com/previews/WB0JJX0KG/tables.html#">Next</a></li>
                    </ul>
                </div>
            </div>
        </article>				
    </div>
</section>
<!-- /Right side -->

</div>
<!-- /Main page container -->

<script src="{?$endereco?}resources/js/foot/main.js"></script>
{?literal?}
<script>
    function sleep(milliseconds) {
        var start = new Date().getTime();
        for (var i = 0; i < 1e7; i++) {
            if ((new Date().getTime() - start) > milliseconds){
                break;
            }
        }
    }

    function log() {
        try {
            console.log.apply( console, arguments );
        } catch(e) {
            try {
                opera.postError.apply( opera, arguments );
            } catch(e){
                alert( Array.prototype.join.call( arguments, " " ) );
            }
        }
    }
                      

    function randomFromTo(from, to){
            return Math.floor(Math.random() * (to - from + 1) + from);
     }


function addEvent(obj, evType, fn, useCapture){
  if (obj.addEventListener){
    obj.addEventListener(evType, fn, useCapture);
    return true;
  } else if (obj.attachEvent){
    var r = obj.attachEvent("on"+evType, fn);
    return r;
  } else {
    alert("Handler could not be attached");
  }
}

    $(document).ready(function(){
    
    
        $('#myTab a:first').tab('show');
        $('#myTab a').click(function (e) {
            e.preventDefault();
            $(this).tab('show');
        })
                                
        
                                     
        
    
    
        /* console.log("foi");
$('html').css({
'overflow-x': 'hidden'
});

if ($('.quiz').length) {
$('.pergunta ul li a').click(function(e){
quiz.clique_resposta($(this));  
var f = "quiz.muda_questao($('.pergunta.current'))";
setTimeout(f,3000)
e.preventDefault();
});
        
setInterval(function(){
        var time = parseInt($('#countdown strong').html());
			
        if (time == 0)
                quiz.muda_questao($('.pergunta.current'));
        else if (time > 0)
                $('#countdown strong').html(time-1);
}, 1000);
        
}*/
                            
                            
                            
                            
   var partida = {
        ativo: true,
        elemento_atual:null,
        publico:null,
        local:null,
        campo : {
            elemento : Object,
            cima : 0,
            baixo : 400,
            direito : 700,
            esquerdo : null
        },
        bola : {
            elemento : Object,
            y : null,
            x : null
        },
        casa : {
            nome:"Parmera",
            gols :2,
            jogadores : [{
                    id : 1,
                    nome : "zezinho",
                    posicao : "atacante",
                    numero : 10, 
                    x : 50,
                    y : 10 
                },
                {
                    id : 2,
                    nome : "robertinho",
                    posicao : "meio",
                    numero : 9,
                    x : 50,
                    y : 10   
                }]
        },
        visitante : {
            nome:"Gambita",
            gols :0,
            jogadores : [{
                    id : 31,
                    nome : "Marcelinho",
                    posicao : "atacante",
                    numero : 10,
                    x : 50,
                    y : 10 
                },
                {
                    id : 4,
                    nome : "Biro Biro",
                    posicao : "meio",
                    numero : 9,
                    x : 50,
                    y : 10 
                }]
        },
	init : function() {
		// set local object vars here
                this.bola.elemento = $("#ball");
                $('#ball').animate({
                // opacity: 0.25,
                left:250,
                top :200
            }, 1000);
                this.campo.elemento = $("#campo");
                
                for (var i in this.casa.jogadores){
                        $("#campo_casa").append('<div id="casa_'+this.casa.jogadores[i].numero+'"><img src="{?/literal?}{?$endereco?}{?literal?}resources/images/foot/Palmeiras-icon.png">'+this.casa.jogadores[i].numero+'</div>',                        $("#casa_"+this.casa.jogadores[i].numero).animate({
                            // opacity: 0.25,
                            left:'+='+this.casa.jogadores[i].x,
                            top :'+='+this.casa.jogadores[i].y
                        }, 1000));
                    }
                    
                    
                 for (var i in this.visitante.jogadores){
                        $("#campo_visitante").append('<div id="visitante_'+this.visitante.jogadores[i].numero+'"><img src="{?/literal?}{?$endereco?}{?literal?}resources/images/foot/Palmeiras-icon.png">'+this.visitante.jogadores[i].numero+'</div>');
                        $("#visitante_"+this.visitante.jogadores[i].numero).animate({
                            // opacity: 0.25,
                            left:'+='+this.visitante.jogadores[i].x,
                            top :'+='+this.visitante.jogadores[i].y
                        }, 1000);
                    }    
                
		this.kickoff();

    $('#casa_20').animate({
                // opacity: 0.25,
                left:250,
                top :200
            }, 1000);
 
	},
	 kickoff: function() {
		// run bulk of behavior here
                $("#conteudo_narracao").append("Inicia a partida aqui no estadio do "+this.casa.nome);
                this.posicao_bola();
	},
        posicao_bola: function(){
             var position = this.bola.elemento.position();
             this.bola.y = position.left;
             this.bola.x = position.top;  
             $("#conteudo_narracao").append( "left: " + position.left + ", top: " + position.top );
        }
    }
    
    
    function initializer() {
            partida.init();
            // other init() methods go here
    }



        function passe(){
            var altura = randomFromTo(-80,80);
            var distancia = randomFromTo(-80,80);
            $('#ball').animate({
                // opacity: 0.25,
                left: '+='+distancia,
                top :'+='+altura
            }, 1000, function() {
                // Animation complete.
                //alert("complete");
                passe();
                partida.posicao_bola();
                if(partida.bola.x < partida.campo.cima || partida.bola.x > partida.campo.baixo){
                    $("#conteudo_narracao").append("<br>lateral"); 
                    
                     $('#ball').animate({
                        // opacity: 0.25,
                        top :0
                    }, 1000);
                    sleep(2000);
                    var altura_fora = randomFromTo(0,80);
                    var distancia_fora = randomFromTo(0,80);
                     $('#ball').animate({
                        left: '+='+distancia_fora,
                        top :'+='+altura_fora
                    }, 1000);
                }else if(partida.bola.y > partida.campo.direito || partida.bola.y < partida.campo.esquerdo){
                    $("#conteudo_narracao").append("<br>fora"); 
                    
                    $('#ball').animate({
                        // opacity: 0.25,
                        left :0
                    }, 1000);
                    sleep(2000);
                     $('#ball').animate({
                        left: '+='+distancia_fora,
                        top :'+='+altura_fora
                    }, 1000);
                }
            });
        }     
                                
        $('#clickme').live('click', function() {
            passe();
             $('#casa_20').animate({      
                        left: '+='+100,
                        top :'+='+100
                    }, 1000);
        });


addEvent(window,'load',initializer);                   
                            
                            
                            
                            
                            
                            
    });

                        
                        
</script>
{?/literal?}
<div class="modal hide" id="myModal">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3>Modal header</h3>
    </div>
    <div class="modal-body">
        <p>One fine body…</p>
    </div>
    <div class="modal-footer">
        <a href="http://wbpreview.com/previews/WB0JJX0KG/tables.html#" class="btn" data-dismiss="modal" aria-hidden="true">Close</a>
        <a href="http://wbpreview.com/previews/WB0JJX0KG/tables.html#" class="btn btn-primary">Save changes</a>
    </div>