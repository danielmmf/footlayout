<!DOCTYPE html>
<html>
    <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>Foot Online</title>
		<meta name="description" content="Jogo de futebol online,estilo manager mas feito socialmente.">
		<meta name="author" content="Daniel Medeiros">
		<meta name="robots" content="index, follow">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
				
		<!-- CSS styles -->
		<link rel="stylesheet" type="text/css" href="{?$endereco?}resources/css/foot/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="{?$endereco?}resources/css/foot/bootstrap-responsive.min.css">
		<link rel="stylesheet" type="text/css" href="{?$endereco?}resources/css/foot/main.css">
                <link rel="stylesheet" type="text/css" href="{?$endereco?}resources/css/foot/foot.css">
		
		<!-- JS Libs -->
		<script src="{?$endereco?}resources/js/foot/jquery.js"></script>
		<script src="{?$endereco?}resources/js/foot/bootstrap.min.js"></script>
		<script src="{?$endereco?}resources/js/foot/jquery.flot.js"></script>
		<script src="{?$endereco?}resources/js/foot/jquery.flot.pie.js"></script>
		<script src="{?$endereco?}resources/js/foot/jquery.flot.resize.js"></script>
	</head>
	<body>
	
		<!-- Main page container -->
		<div class="container-fluid">
		
			<!-- Left side -->
			<section class="navigation-block">
			
				<!-- Main page header -->
				<header>				
					<!-- Main page logo -->
					<a href="http://wbpreview.com/previews/WB0JJX0KG/index.html" title="Back to Homepage"><img alt="" src="./index_files/logo.png"></a>
				</header>
				<!-- /Main page header -->
				
				<!-- User profile -->
				<section class="user-profile">
					<figure>
						<img alt="avatar" >
						<figcaption>
							<strong><a href="http://wbpreview.com/previews/WB0JJX0KG/#" class="">Jogador</a></strong>
							<em>desde 26/10/2012</em>
                                                        <em><br>R$ : 14.000,00</em>
							<ul>
								<li><a class="label" href="http://wbpreview.com/previews/WB0JJX0KG/#">conta</a></li>
								<li><a class="label" href="http://wbpreview.com/previews/WB0JJX0KG/#">logout</a></li>
							</ul>
						</figcaption>
					</figure>
				</section>
				<!-- /User profile -->
				
				<!-- Main navigation -->
				<nav class="main-navigation" role="navigation">
					<ul>
						<li class="current"><a href="http://wbpreview.com/previews/WB0JJX0KG/index.html"><i class="icon-file"></i>Meu Time</a></li>
						<li><a href="http://wbpreview.com/previews/WB0JJX0KG/forms.html"><i class="icon-user"></i>Tabela</a></li>
						<li><a href="http://wbpreview.com/previews/WB0JJX0KG/charts.html"><i class="icon-signal"></i>Pagamentos</a></li>
						<li><a href="http://wbpreview.com/previews/WB0JJX0KG/tables.html"><i class="icon-th-large"></i>Treinos</a></li>						
						<li><a href="http://wbpreview.com/previews/WB0JJX0KG/file_explorer.html"><i class="icon-folder-open"></i>Mensagens<span class="badge" data-original-title="12 tasks this week">12</span></a></li>
						<li><a href="http://wbpreview.com/previews/WB0JJX0KG/calendar.html"><i class="icon-calendar"></i>Calendario<span class="badge" data-original-title="12 tasks this week">12</span></a></li>
						<li>
							<a href="http://wbpreview.com/previews/WB0JJX0KG/#" class="submenu"><i class="icon-heart"></i>conta</a>
							<ul style="display: none; ">
								<li><a href="http://wbpreview.com/previews/WB0JJX0KG/login.html">Login Page</a></li>
								<li><a href="http://wbpreview.com/previews/WB0JJX0KG/403.html">Error 403</a></li>
								<li><a href="http://wbpreview.com/previews/WB0JJX0KG/404.html">Error 404</a></li>
							</ul>
						</li>
					</ul>
				</nav>
				<!-- /Main navigation -->				
			</section>
			<!-- /Left side -->
			
			<!-- Right side -->
                        
                        
                        
			{?include file=$partial?}
                        ...
			<!-- /Right side -->
			
		</div>
		<!-- /Main page container -->
		
		<script src="{?$endereco?}resources/js/foot/main.js"></script>	
                {?literal?}
		<script>
			
                        $(function () {
				$('#myTab a:first').tab('show');
				$('#myTab a').click(function (e) {
					e.preventDefault();
					$(this).tab('show');
				})								
			});
			
			var data = [];
			var series = Math.floor(Math.random()*4)+1;
			for( var i = 0; i<series; i++)
			{
				data[i] = { label: "Series "+(i+1), data: Math.floor(Math.random()*100)+1 }
			}


			var placeholder = $("#chart-div");
			
			var plot = $.plot(placeholder, data,
			{
				series: {
					pie: { 
						show: true
					}
				},
				legend: {
					show: true
				}
			});   
			{?/literal?}		
		</script>
		
</body></html>