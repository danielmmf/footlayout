<div class="row-fluid">
    <div class="span12" id="campo">
        <article class="data-block">							
            <div class="data-container" style="max-width:980px;height: 570px;background-image: url('{?$endereco?}resources/images/foot/gramado_foot.png')">
                <div id="ball" style="background-color: #000000;position: absolute; "><img src="{?$endereco?}resources/images/foot/soccer_ball_icon.gif"></div>
                <div class="row-fluid">
                    <div class="span6" id="campo_casa">
                    </div>
                    <div class="span6" id="campo_visitante" >
                    </div>
                </div>
            </div>
        </article>
    </div>
    <div class="span12">
        <article class="data-block">							
            <div class="data-container">
                <h4>Narração da Partida</h4>
                <div id="conteudo_narracao" style="overflow: auto">
                    <p>Zezinho pega a bola no meio de campo e passa pra Robertinho</p>
                    <p>Robertinho pega a bola e numa corrida avança em direção ao gol</p>
                </div>
                <div id="clickme" class="btn btn-mini btn-primary">
                    Jogada
                </div>
            </div>
        </article>
    </div>
</div>