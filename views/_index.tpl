<section class="content-block" role="main">				
				<!-- Breadcrumbs -->
				<!--ul class="breadcrumb">
					<li>
						<a href="#"><i class="icon-home"></i></a> <span class="divider">/</span>
					</li>
					<li class="active">Dashboard</li>
				</ul-->
				<!-- /Breadcrumbs -->
				<div class="row-fluid">					
					<div class="span12">
						<div class="alert alert-info">
							<button type="button" class="close" data-dismiss="alert">×</button>
							<strong>Olá Daniel !</strong> Isso precisa de atenção mas não é nada importante.
						</div>
					</div>
				</div>				
				<div class="row-fluid">					
					<div class="span8">
						<article class="data-block">
							<div class="block-title"><h4>Meu time -- [Nome do Meu time]</h4></div>
							<div class="data-container">
								<table class="table table-stripped">
									<thead>
										<tr>
											<th>Time</th>
											<th>Pontos</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Aliquam mauris tellus</td>
											<td>45</td>												
											<td rowspan="4">												
												<div id="chart-div" style="padding: 0px; position: relative; "><canvas class="base" width="300" height="150"></canvas>
                                                                                                    <canvas class="overlay" width="300" height="150" style="position: absolute; left: 0px; top: 0px; "></canvas><div class="legend"><div style="position: absolute; width: 0px; height: 0px; top: 5px; right: 5px; background-color: rgb(255, 255, 255); opacity: 0.85; "> </div><table style="position: absolute; top: 5px; right: 5px; font-size: smaller; color: rgb(84, 84, 84); ">
                                                                                                            <tbody><tr><td class="legendColorBox"><div style="border:1px solid #ccc;padding:1px"><div style="width:4px;height:0;border:5px solid rgb(237,194,64);overflow:hidden"></div></div></td><td class="legendLabel">Grupo 1</td></tr><tr><td class="legendColorBox"><div style="border:1px solid #ccc;padding:1px"><div style="width:4px;height:0;border:5px solid rgb(175,216,248);overflow:hidden"></div></div></td><td class="legendLabel">Grupo 2</td></tr><tr><td class="legendColorBox"><div style="border:1px solid #ccc;padding:1px"><div style="width:4px;height:0;border:5px solid rgb(203,75,75);overflow:hidden"></div></div></td><td class="legendLabel">Grupo 3</td></tr><tr><td class="legendColorBox"><div style="border:1px solid #ccc;padding:1px"><div style="width:4px;height:0;border:5px solid rgb(77,167,77);overflow:hidden"></div></div></td><td class="legendLabel">Grupo 4</td></tr></tbody></table></div></div>
											</td>
										</tr>
										<tr>
											<td> Morbi quam orci</td>
											<td>21</td>
										</tr>
										<tr>
											<td>In hac habitasse platea dictumst</td>
											<td>124</td>
										</tr>
										<tr>
											<td>Fat Man</td>
											<td>23</td>
										</tr>
									</tbody>
								</table>								
								<footer class="note">
									<p>Pontos atualizados a todo momento , veja se seu campeonato é de dias ou horas. </p>
								</footer>
							</div>
						</article>
						<article class="data-block">
							<div class="block-title"><h4>Posts</h4></div>
							<div class="data-container">
								<ul class="nav nav-tabs" id="myTab">
									<li class="active"><a href="#msg">Novas mensagens</a></li>
									<li><a href="#comments">Comentarios</a></li>
								</ul>							 
								<div class="tab-content">
									<div class="tab-pane active" id="msg">
										<ul>	
											<li><a href="#msg1">Jogador machucado no treino , ficará dois jogos de fora :: Josicleison</a> <span class="label label-warning">pootz!</span></li>
											<li><a href="http://wbpreview.com/previews/WB0JJX0KG/#">Oferta de troca de jogadores</a> <span class="label label-info">pendente</span></li>
                                                                                        <li><a href="http://wbpreview.com/previews/WB0JJX0KG/#">Apos 12 derrotas Antonio minininho foi despedido do <b>Toca-Fogo</b></a></li>
											<li><a href="http://wbpreview.com/previews/WB0JJX0KG/#">Perspiciatis unde omnis iste natus error sit voluptatem</a></li>
										</ul>
									</div>
									<div class="tab-pane" id="comments">
										<ul>	
											<li><strong>John Smith</strong><br>Coluptatem accusantium doloremque laudantium, totam rem aperiame</li>
											<li><strong>Jenny</strong><br>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</li>				
										</ul>										
									</div>
								</div>
							</div>
						</article>
						<article class="data-block">
							<div class="block-title"><h4>Atividades Recentes</h4></div>
							<div class="data-container">														 								
								<ul class="activity-list">
									<li>
										<a href="http://wbpreview.com/previews/WB0JJX0KG/#"><i class="icon-user"></i><strong>Admin</strong> adicionou <strong>1 user</strong> <span>2 horas atras</span></a>
									</li>
									<li>
										<a href="http://wbpreview.com/previews/WB0JJX0KG/#"><i class="icon-file"></i><strong>John Smith</strong> deletou um  <strong>post</strong> <span>ontem</span></a>
									</li>
									<li>
										<a href="http://wbpreview.com/previews/WB0JJX0KG/#"><i class="icon-envelope"></i><strong>Zelao</strong> comprou o jogador <strong>Marreco</strong> de <strong>Zezinho</strong>por R$90 milhões<span> Um dia atras</span></a>
									</li>
								</ul>
							</div>
						</article>
					</div>
					<div class="span4">
						<article class="data-block">
							<div class="block-title"><h4>Os 4 melhores da rodada nacional</h4></div>
							<div class="data-container">
								<section>
									<table class="table table-stripped table-hover">
										<thead>
											<tr>
												<th>Time</th>
												<th>Pontos</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td><a href="http://wbpreview.com/previews/WB0JJX0KG/#">Parmera</a></td>
												<td>129</td>
											</tr>
											<tr>
												<td><a href="http://wbpreview.com/previews/WB0JJX0KG/#">Avai</a></td>
												<td>118</td>
											</tr>
											<tr>
												<td><a href="http://wbpreview.com/previews/WB0JJX0KG/#">Asa</a></td>
												<td>118</td>
											</tr>
                                                                                        <tr>
												<td><a href="http://wbpreview.com/previews/WB0JJX0KG/#">Real Madrid</a></td>
												<td>116</td>
											</tr>
										</tbody>
									</table>
								</section>
							</div>
						</article>
						<article class="data-block">
							<div class="block-title"><h4>Ultimos Desafios</h4></div>
							<div class="data-container">
								<section>
									<table class="table table-stripped table-hover">
										<thead>
											<tr>
												<th class="avatar">Avatar</th>
												<th>User Name</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td><img alt="cuongv avatar" src="./index_files/avatar0.png" class="img" width="40" height="40"></td>
												<td><a href="http://wbpreview.com/previews/WB0JJX0KG/#">John Smith</a><br><small>2012-07-22 09:12</small></td>
											</tr>
											<tr>
												<td><img alt="cuongv avatar" src="./index_files/avatar1.png" class="img" width="40" height="40"></td>
												<td><a href="http://wbpreview.com/previews/WB0JJX0KG/#">Jenny</a><br><small>2012-07-19 12:12</small></td>												
											</tr>
											<tr>
												<td><img alt="cuongv avatar" src="./index_files/avatar2.png" class="img" width="40" height="40"></td>
												<td><a href="http://wbpreview.com/previews/WB0JJX0KG/#">Tom</a><br><small>2012-07-19 09:05</small></td>
											</tr>										
										</tbody>
									</table>
								</section>
							</div>
						</article>
					</div>
				</div>				
			</section>